Go Brainfuck
============

Simple brainfuck interpreter (written in go).


Installation
------------

Using `go get` command:

```bash
$ go get gitlab.com/aeroxmotion/brainfuck
```


Usage
-----

Run the executable in your terminal:

```bash
$ brainfuck
> # Insert here your brainfuck code :)
```

You can optionally pass a file:

```bash
$ brainfuck example.bf
Hello world!
```
