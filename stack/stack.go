package stack

type Stack struct {
	Indexes []int
}

// Create new Stack
func New() *Stack {
	return &Stack{make([]int, 0)}
}

// Push index into stack
func (s *Stack) Push(index int) {
	s.Indexes = append(s.Indexes, index)
}

// Get actual stack length
func (s *Stack) Len() int {
	return len(s.Indexes)
}

// Remove last index
func (s *Stack) Pop() {
	s.Indexes = s.Indexes[:s.Len() - 1]
}

// Get the last index
func (s *Stack) Last() int {
	return s.Indexes[s.Len() - 1]
}
