package interpreter

import (
	o "gitlab.com/aeroxmotion/brainfuck/output"
	s "gitlab.com/aeroxmotion/brainfuck/stack"

	"os"
	"io/ioutil"
	"bufio"
	"fmt"
)

type Interpreter struct {
	Data 	[]byte // Byte cells
	Pointer uint16 // Data pointer
}

// Create new Interpreter
func New() *Interpreter {
	return &Interpreter{
		make([]byte, DATA_SIZE),
		LEFTMOST_BYTE,
	}
}

// Set pointed byte
func (i *Interpreter) SetByte(value byte) {
	i.Data[i.Pointer] = value
}

// Get pointed byte
func (i *Interpreter) GetByte() byte {
	return i.Data[i.Pointer]
}

// Increment pointer value
func (i *Interpreter) IncrementPointer() {
	i.Pointer++
}

// Decrement pointer value
func (i *Interpreter) DecrementPointer() {
	i.Pointer--
}

// Increment pointed byte
func (i *Interpreter) IncrementByte() {
	i.SetByte(i.GetByte() + 1)
}

// Decrement pointed byte
func (i *Interpreter) DecrementByte() {
	i.SetByte(i.GetByte() - 1)
}

// Input byte from Stdin
func (i *Interpreter) InputByte() {
	input := make([]byte, 1)
	os.Stdin.Read(input)

	i.SetByte(input[0])
}

// Evaluate given `instructions`
func (i *Interpreter) Eval(instructions []byte) (string, error) {
	// Instructions length
	length := len(instructions)

	// Output buffer
	output := o.New()

	// Stack of jumps
	stack := s.New()

	// Iterate over all instructions
	for index := 0; index < length; index++ {
		switch instructions[index] {
		case INCREMENT_POINTER:
			i.IncrementPointer()
		case DECREMENT_POINTER:
			i.DecrementPointer()
		case INCREMENT_BYTE:
			i.IncrementByte()
		case DECREMENT_BYTE:
			i.DecrementByte()
		case OUTPUT_BYTE:
			// Write pointed byte in the output
			output.Write(i.GetByte())
		case INPUT_BYTE:
			fmt.Print("Input byte: ")
			i.InputByte()
		case JUMP_FORWARD:
			if i.GetByte() != 0 {
				stack.Push(index)
			} else {
				for depth := 1; depth > 0; {
					index++

					if index == length {
						return "", MISMATCHED_PARENTHESES_ERROR
					}

					switch instructions[index] {
					case JUMP_BACK:
						depth--
					case JUMP_FORWARD:
						depth++
					}
				}
			}
		case JUMP_BACK:
			if stack.Len() == 0 {
				return "", MISMATCHED_PARENTHESES_ERROR
			}

			if i.GetByte() != 0 {
				index = stack.Last()
			} else {
				stack.Pop()
			}
		}
	}

	return output.String(), nil
}

// Evaluate given file
func (i *Interpreter) EvalFile(filename string) (string, error) {
	content, err := ioutil.ReadFile(filename)

	if err != nil {
		return "", err
	}

	return i.Eval(content)
}

// Evaluate input prompt
func (i *Interpreter) EvalInput() (string, error) {
	reader := bufio.NewReader(os.Stdin)
	instructions, _, err := reader.ReadLine()

	if err != nil {
		return "", err
	}

	if string(instructions) == "exit" {
		os.Exit(0)
	}

	return i.Eval(instructions)
}
