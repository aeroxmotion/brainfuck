package interpreter

import "errors"

// Configuration
const (
	// Initial pointer value
	LEFTMOST_BYTE = 0

	// Initial and maximum data size (uint16 + 1)
	DATA_SIZE = 65536
)

// Instructions
const (
	// Increment the data pointer (to point to the next cell to the right).
	INCREMENT_POINTER = '>'

	// Decrement the data pointer (to point to the next cell to the left).
	DECREMENT_POINTER = '<'

	// Increment (increase by one) the byte at the data pointer.
	INCREMENT_BYTE = '+'

	// Decrement (decrease by one) the byte at the data pointer.
	DECREMENT_BYTE = '-'

	// Output the byte at the data pointer.
	OUTPUT_BYTE = '.'

	// Accept one byte of input, storing its value in the byte at the data pointer.
	INPUT_BYTE = ','

	// If the byte at the data pointer is zero, then instead of moving the instruction
	// pointer forward to the next command, jump it forward to the command after the
	// matching ] command.
	JUMP_FORWARD = '['

	// If the byte at the data pointer is nonzero, then instead of moving the instruction
	// pointer forward to the next command, jump it back to the command after the matching
	// [ command.
	JUMP_BACK = ']'
)

// Custom errors
var (
	// When a '[' is not followed by ']' or vice versa
	MISMATCHED_PARENTHESES_ERROR = errors.New("mistmatched parentheses")
)
