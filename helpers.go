package main

import (
	"fmt"
	"os"
)

// Handle the returned result by the `Eval` interpreter method
// to print a correct message
func printEvaluationResult(output string, err error) {
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	} else if output != "" {
		fmt.Println(output)
	}
}
