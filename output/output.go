package output

type Output struct {
	Buffer []byte
}

// Create new Output
func New() *Output {
	return &Output{make([]byte, 0)}
}

// Write into buffer
func (o *Output) Write(value byte) {
	o.Buffer = append(o.Buffer, value)
}

// Get written buffer as string
func (o *Output) String() string {
	return string(o.Buffer)
}
