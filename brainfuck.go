package main

import (
	"gitlab.com/aeroxmotion/brainfuck/interpreter"

	"os"
	"fmt"
)

const BANNER = `
  Brainfuck interpreter v0.1.0 by Camilo Rodríguez <aeromotiondesign@gmail.com>

  Type "exit" to exit.

  Instructions:

    > Increment the data pointer (to point to the next cell to the right).
    < Decrement the data pointer (to point to the next cell to the left).
    + Increment (increase by one) the byte at the data pointer.
    - Decrement (increase by one) the byte at the data pointer.
    . Output the byte at the data pointer.
    , Accept one byte of input, storing its value in the byte at the data pointer.
    [ If the byte at the data pointer is zero, then instead of moving the instruction pointer forward to the next command, jump it forward to the command after the matching ] command.
    ] If the byte at the data pointer is nonzero, then instead of moving the instruction pointer forward to the next command, jump it back to the command after the matching [ command.

  Note: Non-instructions will be ignored.
`

func main() {
	i := interpreter.New()

	if len(os.Args) > 1 {
		output, err := i.EvalFile(os.Args[1])
		printEvaluationResult(output, err)

		// Exit on error
		if err != nil {
			os.Exit(1)
		}
	} else {
		fmt.Println(BANNER)

		for {
			fmt.Print("> ")
			printEvaluationResult(i.EvalInput())
		}
	}
}
